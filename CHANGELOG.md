# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Changed
- Archive scanning will no longer scan the cache directory. You can specify it manually if you want to.

### Fixed
- Fixed archive scanning when the directory to scan is not specified so that it will scan the current working directory

## [0.2.2] - 2022-09-15

### Changed
- Updated to work with portmod 2.1 and newer

### Fixed
- Fixed archive scanning ignoring files from all but the last prefix (and prefixes are
  no longer necessary for archive scanning to work)
